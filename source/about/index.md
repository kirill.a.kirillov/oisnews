---
title: О сайте
layout: page
comments: false
---

## Газета Автомобильно-дорожного института ГОУВПО "Донецкий национальный технический университет"

Учредитель - АДИ ДонНТУ

Главный редактор Семененко В.В.

### Адрес редакции: 
84646,
г. Горловка, 46,
ул. Кирова, 51, Центр ОС
Телефон: 55-20-26

E-mail: ois@adudonntu.ru

