---
title: Контакты
layout: page
comments: false
---

**Автомобильно-дорожный институт ГОУВПО «Донецкий национальный технический университет»**   
<i class="fas fa-address-card"></i>&emsp;84646, г. Горловка, ул. Кирова, 51

**Гуменюк Михаил Михайлович**   
доцент кафедры «Математическое моделирование», ответственный секретарь   
Кафедра «Математическое моделирование»   
<i class="fas fa-phone"></i>&emsp;Телефон: +38 (071) 412-79-07   
<i class="fas fa-envelope-open-text"></i>&emsp;E-mail: misha_gumenyuk@mail.ru

**Курган Наталья Федоровна**   
начальник редакционно-издательского отдела, ответственная за выпуск   
Редакционно-издательский отдел, ауд. 1-104   
<i class="fas fa-phone"></i>&emsp;Телефон: +38 (071) 331-45-58   
<i class="fas fa-envelope-open-text"></i>&emsp;E-mail: vestnik-adi@adidonntu.ru, druknf@rambler.ru   

<figure class="image is-16by9">
  <iframe class="has-ratio" width="640" height="360" src="https://yandex.ua/map-widget/v1/?um=constructor%3A5b0ce4a62101b0a8cf3b5b2860572756ce2b1e0754a9b7d4beddea60de75cb31&amp;source=constructor" frameborder="0" allowfullscreen></iframe>
</figure>